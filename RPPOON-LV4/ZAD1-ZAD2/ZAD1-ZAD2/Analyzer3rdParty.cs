﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1_ZAD2
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            for (int i = 0; i < rowCount; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }
        public double[] PerColumnAverage(double[][] data)
        {
            int Rowcount = data.Count();
            int colum = data[0].Count();
            double[] results = new double[colum];

            for (int i = 0; i < colum; i++)
            {

                for (int j = 0; j < Rowcount; j++)
                {
                    results[i] += data[j][i];
                }
                results[i] = results[i] / data.Length;
            }

            return results;
        }
    }
}
