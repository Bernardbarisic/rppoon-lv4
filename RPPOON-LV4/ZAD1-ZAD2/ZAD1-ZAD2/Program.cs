﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1_ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset datasetToReadFile=new Dataset("C:\\Users\\Bernard\\Desktop\\RPPOON-L4\\ZAD1-ZAD2\\ZAD1-ZAD2\\CSV.txt");
		IAnalytics analyzer=new Adapter(new Analyzer3rdParty());
		double[] averagePerColumn=analyzer.CalculateAveragePerColumn(datasetToReadFile);
		double[] averagePerRow=analyzer.CalculateAveragePerRow(datasetToReadFile);
		Console.WriteLine("The average of rows is:");
		foreach (double number in averagePerRow)
			Console.Write(number+" ");
        Console.WriteLine("");
		Console.WriteLine("The total average of all rows is: "+averagePerRow.Average());
		Console.WriteLine("The average of columns is:");
		foreach (double number in averagePerColumn)
			Console.Write(number+" ");
        Console.WriteLine("");
		Console.WriteLine("The total average of all columns is: "+averagePerColumn.Average());
	    }
    }
}
