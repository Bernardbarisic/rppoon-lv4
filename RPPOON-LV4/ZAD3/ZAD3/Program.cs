﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> list = new List<IRentable>();
            Video video = new Video("Hulk");
            Book book = new Book("Hamlet");
            list.Add(video);
            list.Add(book);
            RentingConsolePrinter rent = new RentingConsolePrinter();
            rent.DisplayItems(list);
            Console.WriteLine("Total price is: ");
            rent.PrintTotalPrice(list);
        }
    }
}
